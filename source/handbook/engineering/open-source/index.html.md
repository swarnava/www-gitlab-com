---
layout: markdown_page
title: "Open Source at GitLab"
---

## We believe in Open Source

As a company, GitLab is dedicated to open source. Not only do we believe in it, but we use it, and we give back to it. Not just through GitLab, but through contributions to other open source projects.

This page describes how we do it.

## General notes

- If you use your work email, this could help promote the company by showing that GitLab is giving back to the open source community reflecting our values of [Collaboration](/handbook/values/#collaboration) and [Transparency](https://about.gitlab.com/handbook/values/#transparency).

TBD
- Mention Intellectual Property https://about.gitlab.com/handbook/people-group/code-of-conduct/#intellectual-property-and-protecting-ip ?
- Mention https://about.gitlab.com/handbook/contracts/#piaa-agreements ?

## Creating an open source project

TBD
- Which license to use
- Where to publish
- Any approvals needed?
- Readme
    - Code of conduct
    - License
    - How to contribute

## Contributing to a third party project

### Contributor License Agreements (CLAs)

TBD
- What is the process to follow to sign a CLA or corporate CLA?
- Do we track which corporate CLAs have been signed already? Would be great to have a link to the list here.

### Contributing to a project on GitLab

TBD
- Fork into personal profile or do we want to have a place for forks, owned by GitLab?

### Contributing to a project on GitHub

If your GitHub account's primary email is not your @gitlab.com email, you can add it as an additional address. No need to create a separate account.

TBD
1. Fork the repository you want to contribute to into the [GitLab organization](https://github.com/gitlab). Using a single organization will allow us to track various metrics about contributions: who, to what repositories, how often.
2. Create a branch with the proposed change
3. Submit a pull request and get it merged.
4. Delete the branch in the fork.
5. If you do not anticipate the need to make more contributions in the near future, consider deleting the fork too.

TBD
- How to join the GitHub org? 

## Using open source libraries

TBD
Incorporate https://gitlab.com/gitlab-org/gitlab/blob/master/doc/development/licensing.md ?

### Using forks in your code

Avoid using forked code and try to contribute your change upstream.

It's typical for forks to fall far behind the upstream repository and such dependencies become a source of pain:
- Rebasing the branch may become non-trivial and it'd become hard to bring such dependency up to date. 
- Some other library in your project might depend on the original version, creating a [diamond dependency problem](https://en.wikipedia.org/wiki/Dependency_hell).

TBD
- What if you have to? Open an issue to track the remediation work and link it in code somewhere? The tracking issue must have a link to the PR/MR upstreaming the code and describe any work that needs to happen to get rid of the forked dependency.
