Gitlab.configure do |config|
  config.endpoint = 'https://gitlab.com/api/v4'
  config.private_token = ENV.fetch('PRIVATE_TOKEN', nil)
end

WWW_GITLAB_COM_PROJECT_ID = 7764
